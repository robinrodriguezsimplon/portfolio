var main = function () {

    jQuery(function($) {
  
        // Function which adds the 'animated' class to any '.animatable' in view
        var doAnimations = function() {
          
            // Calc current offset and get all animatables
            var offset = $(window).scrollTop() + $(window).height(),
                $animatables = $('.animatable');
                $bar = $('.progress-bar');
                $project_title = $('.project-title ');
          
            // Unbind scroll handler if we have no animatables
            if ($animatables.length == 0) {
                $(window).off('scroll', doAnimations);
            }
          
            // Check all animatables and animate them if necessary
            $animatables.each(function(i) {
                var $animatable = $(this);
                if (($animatable.offset().top + $animatable.height() - 20) < offset) {
                    $animatable.removeClass('animatable').addClass('animated');
                    setTimeout(() => {
                        $animatable.removeClass('animated');
                    }, 2000);
                }
                if ((($bar.offset().top + $bar.height() - 20) < offset) || (($project_title.offset().top + $project_title.height() - 20) < offset))  {
                    setTimeout(() => {
                        $bar.addClass('progress-bar-danger');
                    }, 1000);
                }
            });
        };
        
        // Hook doAnimations on scroll, and trigger a scroll
        $(window).on('scroll', doAnimations);
        $(window).trigger('scroll');
      
    });

    // Project Fancy Box
    var pages = [
        'tic-tac-toe',
        'crying-pika'
    ]
    
    for (const index in pages) {
        $( "#project" ).on( "click", `#${pages[index]}`, function() {
    
            $.fancybox.open({
                src : `assets/projet/${pages[index]}/src/index.html`,
                type : 'iframe',
            });
        
        });
    }

    // Leaflet
    $( window ).on("load", function() {
        var mymap = L.map('mapid').setView([45.173861, 5.730778], 12);
      
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox.streets'
        }).addTo(mymap);
        L.marker([45.173861, 5.730778]).addTo(mymap)
    });

    // for (var i=1; i<101; i++){
    //     if (i % 3 == 0 && i % 5 == 0){console.log("FizzBuzz")}
    //     else if (i % 3 == 0){console.log("Fizz")}
    //     else if (i % 5 == 0){console.log("Buzz")}
    //         else {console.log(i)}
    // }

}
document.addEventListener('DOMContentLoaded', main);